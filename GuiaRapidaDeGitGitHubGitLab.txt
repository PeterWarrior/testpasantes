### Guia rapida de Git - GitHub - GitLab ###

##1.Instalacion y configuracion

Enlace ==> http://git-scm.com

$ git --version
$ git config --global user.name "elpedroski"
$ git config --global user.mail "pjgh.apn@gmail.com"
$ git config --list
$ git help add ==> (git help se utiliza con cualquier comando para obtener ayuda)

******************************************************************************************************************************************************************************

##2. Git Workflow - Iteración Basica

Working Area o Working Directory  ============>  Staging area  ===============>  Repository
                                   git add -A                   git commit -m

******************************************************************************************************************************************************************************

##3. Iteracion basica 

$ git add (file or directory) o -A
$ git commit -m "mensaje"
$ git status ==> muestra archivos preparados para la iteracion basica y cuales puedo ir guardando en el repositorio
$ git init ==> inicia el repositorio en git
$ git log
$ git log --online
$ git log --graph
$ git log --all
$ git config --global alias.megalog "log --online --graph --all"
$ git config --global alias.superlog "log --graph --abbrev-commit --decorate --date=relative --format=format:'%C(bold blue)%h%C(reset) - %C(bold green)(%ar)%C(reset) %C(white)%s%C(reset) %C(dim white)- %an%C(reset)%C(bold yellow)%d%C(reset)' --all"

*********************************************************************************************************************************************************************************

##4. Reset

Nota: Se modificó el archivo index.html el "page-title" tres veces, colocando ("1","2" y "123") y a c/u se le hizo iteración básica

## Hard = Working Directory = Staging Area = Repository

$ git reset --hard [sha commit] ==> (regresa todo al punto estable del proyecto)
$ git reset --hard [ultimo commit estable] ==> (ultimo commit estable, borrando los otros tres)
$ git reset --hard [sha commit 123] ==> (regresan los commits "1","2" y "123") *ojo hay que tener guardados los commit id para poder restaurarlos*

## Mixed : Staging Area = Repository. Nada con Working Directory o Working Area

$ git reset --mixed (sha commit]  ==> (agrupamos varios commits en uno solo)
$ git reset --mixed [ultimo commit estable] ==> (la pagina web mantiene el "page-title" con "123", pero en log los commits "1","2" y "123" desaparecieron)
$ git status ==> (muestra el index.html modificado porque el repository y el staging area se acaban de igualar, pero nada ha pasado en el working directory)
$ git add -A ==> (Se requiere hacer iteracion basica)
$ git commit -m "Suma de commit 1 , 2 , 123"
$ git reset --mixed [sha commit 123] ==> (regresan los commits "1","2" y "123", pero borro el commit "Suma de commit 1,2,123")

## Soft : Cambios solo en el Repository. Staging Area = Working Area

$ git reset --soft [sha commit] ==> (a diferencia del git reset --mixed, aqui los archivos estan listos para hacer commit)
$ git reset --soft [ultimo commit estable] ==> (la página web mantiene el "page-title" con "123", los archivos estan listos para hacer un commit)

## Otro ejemplo con git checkout [sha commit]

$ git reset --hard [commit 123]
$ git checkout [ultimo comit estable] ==> (la pagina web muestra el "page-title" "Pedro Guerrero"
$ git checkout [sha commit 123] ==> (regresamos de nuevo a los commits "1","2" y "123" la pagina web muestra "123")
$ git checkout [sha commit base] ==> (regreso al inicio del proyecto)

## Ramas ==> es una línea alterna del tiempo, en la historia de nuestro repositorio. IDEAS-FEACTURES-BUG FIXES

## Fusiones ==> [git checkout master | git checkout rama a fusionar]

Fast-Forward ==> los gestores trabajan archivos diferentes al repositorio
Manual Merge ==> cuando 2 desarrolladores trabajan el mismo archivo. En la fusion (mismas líneas de código)

## Rebase ==> con rebase la rama experimental se pone delante de master, se ejecuta muy poco en el proyecto

*******************************************************************************************************************************************************************************

##5. Proyecto con líneas de tiempo alternas

$ git init
$ git add -A 
$ git commit -m "0"
$ touch 1.txt
$ touch 2.txt ==> git add -A | git commit -m "1" "2" "3" (para c/u)
$ touch 3.txt
$ git checkout -b experimental ==> (crea una rama llamada experimental y nos ubica en ella)
$ touch a.txt
$ touch b.txt ==> git add -A | git commit -m "a" "b" "C" (para c/u)
$ touch c.txt 
$ git log
$ git superlog
$ git checkout master
$ touch 4.txt
$ git add -A
$ git commit -m "4"
$ git merge experimental ==> se coloca en el archivo que se abre "Fusion realizada" salvar y salir
$ git log
$ git superlog
$ git branch -d experimental ==> borra la rama experimental, pero primero debe haberse fusionado
$ touch x
$ touch y ==> git add -A | git commit "x" (para c/u)
$ touch z 

$ git status
$ git checkout -b experimental2 ==> Desde atom editamos el archivo "x" agregamos "hola mundo" y salvamos
$ git add -A 
$ git commit -m "hola mundo"
$ git checkout master 

Nota: Desaparece "hola mundo" del archivo "x", porque estamos en la rama master. Agregamos "querido mundo"

$ git add -A
$ git commit -m "querido mundo"
$ git merge experimental2 ==> Al fusionar se genera un conflicto, porque se esta trabajando la misma linea de codigo. El editor atom muestra resaltado el problema y debemos 
                              escoger cual borrar si "hola mundo" o "querido mundo"
$ git status
$ git log
$ git superlog ==> Observo la fusion
de 
## Valor agregado

$ git commit -am "Descripcion cambiada" --amend ==> Rectifica el último commit, --amend solo funciona cuando realizamos cambio en el ultimo commit

$ ls -la ==> muestra carpetas ocultas
$ rm -rf .git/ ==> Ojo borramos todo el registro del proyecto. Es arrancar desde cero.

********************************************************************************************************************************************************************************
 
##6. Proyecto marca personal

$ git init
$ git status 
$ git add -A
$ git commit -m "0 - Base del proyecto de marca personal"
$ touch abc.txt
$ git add -A
$ git commit -m "Agregamos abc"
$ git superlog
$ git checkout -b feacture1
$ touch xyz.txt
$ git add -A
$ git commit -m "Agregamos xyz"
$ git superlog 
$ git checkout -b subfeacture1
$ touch lmn.txt
$ git add -A
$ git commit -m "Agregamos lmn"
$ git superlog
$ git checkout feacture1
$ touch def.txt
$ git add -A
$ git commit -m "Agregamos def"
$ git checkout master
$ touch archivofinal.txt
$ git add -A
$ git commit -m "archivofinal"
$ git superlog

*******************************************************************************************************************************************************************************

##7. Head Pointer + git log

$ ls -la
$ git rm -rf .git/
$ git init
$ git add -A
$ git commit -m "Archivos bases instalados"

Nota: Se modifico el index.html salvo y salgo

$ git commit -am "Cambio de Ingles a Español"
$ git status ==> Pero debo volver a corregir el index.html
$ git add -A
$ git commit -m "Plantilla cambiada de Español a Ingles" --amend ==> con --amend se corrige el ultimo commit creado

## git log

$ git log --oneline ==> muestra id y el comentario
$ git log --decorate ==> muestra el head en el ultimo commit
$ git checkout [commit id] ==> cambia el head al commit indicado
$ git checkout master ==> el head regresa a la rama master
$ git log --stat ==> explica al detalle el numero de lineas cambiadas
$ git log -p ==> es un analisis mas profundo del anterior
$ git shortlog ==> agrupa por autor y muestra los titulos de commits. Sabemos en que trabaja cada desarrollador
$ git log --graph --oneline --decorate ==> crea una grafica que permite seguir la historia del proyecto
$ git log --pretty=format:"%cn hizo un commit %h el dia %cd" ==> muestra mensajes personalizados de los commits
$ git log -2 ==> muestra los 2 ultimos commits
$ git log --after="today"
$ git log --before="today"
$ git log --author="elpedroski"
$ git log --grep="mensaje" -i ==> el -i no diferencia entre minisculas y mayusculas
$ git log --index.html
$ git log -S"git" ==> busca dentro del contenido de las carpetas

********************************************************************************************************************************************************************************

##8. Workflows

- Flujos de trabajo colaborativo
- GitHub: es un servicio de hosting de repositorios, a traves de una interfaz web 
- Funciona en 2 bases: exploración (clone) y colaboración (remote)

## GitHub exploracion (clone): descarga un proyecto con el objetivo de utilizarlo sin pensar en colaborar

Nota: Buscamos en GitHub el repositorio github/gemoji y lo clonamos 

$ git rm -rf .git/
$ git init
$ git add -A
$ git commit -m "Base de archivos"
$ git clone [repositorio via ssh o https]

## GitHub colaboracion. Proyecto creado por ti - Repositorios personales

- Eres propietario del proyecto
- Personas alrededor proponen y tu decides si aceptar o no
- Proyectos personales generalmente

Ejercicio: 

- Crear usuario en GitHub
- Crear repositorio 
- Conectar con llave ssh a GitHub desde tu area local

Paso a paso:

1. Nos dirigimos a GitHub (elpedroski)
2. Clic en repositorio. Clic en boton verde (New)
3. Colocar nombre (ejercicio), tildar public, crear repositorio
4. Ir a setting ==> menu a la derecha donde esta profile
5. Ir a ssh and gpg keys ==> menu a la izquierda
6. Crear llave ssh ahora vamos a la consola
$ ssh-keygen -t rsa -b 4096 -C "pjgh.apn@gmail.com"  enter
  .ssh/id_rsa): enter por default
  passphrase: enter por default (sin contraseña por el ejercicio)
  passphrase again: enter por default

Nota: muestra una imagen en la consola, muestra una key fingerprint y un esquema [rsa 4096]. Con esto tenemos creada la llave publica y privada. Vamos a la raiz de home
      directory y ubicamos una carpeta .ssh
$ cd .ssh/
$ ls -la
$ cat id_rsa.pub ==> llave publica desde consola

Nota: copiamos el contenido del archivo y lo pegamos en New ssh keys en GitHub. Ahora vamos a nuestro repositorio recien creado (ejercicio)

$ mkdir prueba
$ cd prueba
$ touch archivo1.txt
$ git init 
$ git add -A
$ git commit -m "Base de archivos"
$ git remote add origin [ssh:repositorio ejercicio]
$ git push origin master

## Proyectos creados por tu organizacion o equipos

- Son propietarios del proyecto
- Todos suben cambios sin pedir permisos
- Siempre verificar cambios nuevos antes de subir los propios

$ git remote add origin [ssh]
$ git fetch origin ==> descarga los cambios a la rama origin/master
$ git merge origin/master ==> realizamos la fusion siempre desde la rama master jalo la rama origin/master
  **desarrollamos**
$ git fetch origin 
$ git merge origin/master
$ git push origin master

********************************************************************************************************************************************************************************

##9. Repositorios remotos (FORKED). Repositorios de terceros

- No son dueños, pero quieren colaborar en el desarrollo
- Los repositorios aceptan y rechazan propuestas a traves de (Pull Request)
- Existen 2 repositorios: el repositorio original y el repositorio forked (la replica del original en tu cuenta GitHub)

Ejercicio: proceso de repositorio "FORKED"

- Actualizar siempre con el reposotorio principal, antes de comenzar
- Desarrollar y, antes de subir a nuestro repositorio forked, revisar primero si hay cambios
- Subir al repositorio forked todo lo que hemos hecho 
- Ejecutar un Pull Request

Nota: crear o entrar a la carpeta del proyecto

$ git remote add origin [https o ssh del proyecto forked]
$ git remote add upstream [https o ssh del proyecto principal]
$ git fetch upstream
$ git merge origin/upstream
$ git fetch origin
$ git fetch origin/master
**hacer cambios en local**
$ git fetch upstream
$ git merge origin/upstream
$ git push origin master

## Ejercicio final

1. Desde mi cuenta GitHub busco a mikecolombiano
2. Le doy clic a fork (boton a la derecha superior)
3. Aparece en pantalla: elpedroski/impresionante forked from mikecolombiano/impresionante
4. Creo una carpeta en consola
$ mkdir impresionante
$ git init
$ git remote add origin [ssh elpedroski/impresionante "repo forked"]
$ git remote add upstream [ ssh mikecolombiano/impresionante "repo original"]
$ git remote -v
$ git fetch origin ==> bajo los archivos del "repo forked"
$ git merge origin/master ==> ahora tengo los archivos en mi carpeta impresioanante local
$ git fetch upstream ==> bajo los archivos de "repo original"
$ git merge upstream/master ==> se sincronica lo que hay en "repo original" con "repo forked"
$ touch vzla.txt
$ git add -A
$ git commit -m "Propuesta Venezuela"
$ git push origin master

Nota: Desde GitHub elpedroski/impresionante presiono el boton "New Pull Request" y aparece "Comparing Changes", entonces presiono "Create Pull Request", escribo algún comentario
      y presiono otra vez el boton "Create Pull Request". Vamos a la cuenta de mikecolombiano/impresionante y observo que se genero un "Pull Request",esperando la aceptación.
 
*********************************************************************************************************************************************************************************

##10. Gestión de proyectos en git

Temario:

- Project Management
- Issues
- Milestones
- Tags
- Usuarios
- Pull Request en equipos

Paso a paso:

## Project Management en GitHub

1. Crear organización desde GitHub ==> pestaña superior a la derecha desplegar settings / menu a la izquierda - Organizations / New organizations - colocar nombre : "elpedroski
                                       -universidadplatzi"
2. Colocar correo ==> "pjgh.apn@gmail.com"
3. Presionar boton crear organization (option free)
4. Invitar miembros por username
5. Presionar boton finish
6. Opciones Settings ==> Member Privileges / Repository Permissions / Read Team / Team Discussions
7. Opciones People ==> Private / Public
8. Invito a guerreroski a participar en el proyecto "elpedroski-universidadplatzi", el usuario debe aceptar en su correo personal la invitacion
9. Crear nuevo repositorio en "elpedroski-universidadplatzi"
10. Colocar nombre al repositorio, siguiendo al ejercicio "blog-universidad"
11. Descripción: "Es un proyecto dedicado al crecimiento de la comunidad universitaria a traves de un blog de eventos"
12. Permitir que el repositorio sea Public ==> recomendado
13. Inicializar el repositorio con un README
14. Clic a boton Crear Repositorio Organizacional
15. Opciones Settings del repositorio organizacional ==> Collaborations & teams: agrego usuarios colaboradores y verifico
16. Crear nuevo equipo: Frontend / descripcion: "Desarrollo de toda la capa frontend del proyecto / team visibility: visible / clic a boton crear equipo
17. Vamos a repositorio "blog-universitario" clic en edit ==> ubicado arriba a la derecha, donde se muestra la descripcion del repositorio 
18. En website ==> colocamos la direccion del proyecto para efecto del curso colocamos: http://universidadplatzi.com salvamos
19. Clic al README del repositorio "blog-universitario" ==> enter a edit y colocamos toda la informacion del proyecto (siguiendo buenas practicas)
    Nombre del proyecto
    [Descripcion del proyecto]
    [Instalacion]
     -Requisitos
     -Version
     -Encargados del proyecto
    [Uso]
    [Documentaciom]
    [Roadmap]
    [Licencia]
    Commit Changes: Desarrollo de la documentacion 
    Se agrego
     - Docuemntacion, Roadmap, Licencia
20. Clic a commit changes

## Issues - Milestones en GitHub

21. elpedroski-universidadplatzi/blog-universidad
22. Pestaña Issues ==> estan todos los track (bugs,feacture,request)
23. Pestaña Milestones ==> son las lineas de progreso
*24. Crear un Milestones
    Titulo:"Desarrollo de Landing"
    Descripcion: Crear la pagina de aterrizaje para que los usuarios entren en contacto directo con:
                 - Newsletter 
                 - Redes sociales
                 - Descripcion
                 - Nombre del proyecto
    Fecha: Colocar fecha tentativa de entrega del proyecto
    clic en "Create Milestones"
25. Pestaña Labels ==> Dejamos todo como esta
26. Pestaña Issues/New Issue ==> Colocar todo lo que se necesita para crear el "Landing"
27. Creación de Newsletter
    Writte: 
    - Un boton de suscripcion
    - Descripcion (copywriting)
    - Este conectado con Mailchimp
28. Labels ==> enhancement
    Assignees ==> guerreroski
29. Clic en submit New Issue
30. Clic en pestaña Issue ==> muestra la creación de Newsletter
*31. Crear otro Milestones 
     Titulo:"Desarrollo de los sitios post"
     Descripcion: 
                 - Encabezados, datos y contenido del articulo
                 - Area de autores
                 - Sistema de comentarios
                 - Hedden (navegacion)
     Fecha: Colocar la fecha tentativa de entrega del proyecto
     clic en "Create Milestones"
*32. Crear otro Milestones
     Titulo: "Crear repositorio"
     Descripcion: 
                 - Creacion de repositorios en GitHub
                 - Asignacion de responsabilidades por miembro
     Fecha: Colocar la fecha tentativa de entrega del proyecto
33. Vamos a crear un New Issue en el Milestone "Crear repositorio"
    Titulo: Creacion de repositorio "blog-universidad"
    Assignes: elpedroski
    Labels: enhancement
    Milestones: "Crear repositorio"
    Clic en submit New Issue

## Como manejar Pull Request

Nota: https://imgur.com ==> plataforma para subir imagenes | wireframes y mockups ejemplos | para editar las paginas creadas y agregar imagenes
    
34. En la pestaña Wiki ==> se hace todo lo anterior
35. En la pestaña Insights ==> se muestra toda la actividad del proyecto
*36 Desde la cuenta GitHub Guerreroski ==> hago un Fork a elpedroski-universidadplatzi/blog-universidad
37. Creo un New File
    Edit: Hola Mundo
    Clic en commit New File ==> se muestra index.html
38. Editamos el README.md ==> agregamos comentario
    Comentario: [agradecimiento]
    Commit Changes enter
39. Seguimos en guerreroski y ahora voy hacer un New Pull Request ==> es solicitar permiso a elpedroski, para agregar los commits trabajados

Nota: En la pestaña Split observo al detalle las modificaciones 

40. Clic a Create Pull Request
    Titulo: Mejora de documentacion
    Write: Se agrego agradecimiento
    Clic a Create Pull Request  
41. Ahora desde la cuenta GitHub "elpedroski" voy al repositorio elpedroski-universidadplatzi/blog-universidad
    Observo que hay un Pull Request "Mejora de documentacion"
42. Write: Me parece excelente. Todo bien
    Clic a Comment
    Clic a Merge Pull Request

## Hostings Recomendados

- www.webfaction.com ==> ideal para proyectos pequeños   -------------
- www.digitalocean.com                                               -
- www.hostgator.mx                                                   - ==> se paga de acuerdo al consumo o crecimiento del proyecto
- https://aws.amazon.com ==> ideal para proyectos grandes ------------
  
*********************************************************************************************************************************************************************************
 
##11. Wokflow Dinamicos. Creacion de Multiusuarios en local
 
## Creacion de llaves

$ ssh-keygen -t rsa -C "peterjgh@gmail.com" ==> dar enter a todo por default
$ ssh-keygen -t rsa -C "pedroskiplgo@gmail.com" ==> dar enter a todo, pero le coloco a esta llave el nombre de "guerreroski"

## Ahora agregamos las llaves a GitHub

/.ssh$ cat id_rsa.pub ==> copio el contenido a New SSH Key (titulo de la llave en GitHub "elpedroski-ven")
/.ssh$ cat guerreroski.pub ==> copio esta llave en GitHub "guerreroski" New SSH Key (titulo de la llave "guerreroski-col")

/.ssh $ touch config
/.ssh $ nano config
# githubCuenta1
   Host cuenta1
        HostName github.com
        User git
        IdentityFile ~/.ssh/id_rsa

# githubCuenta2
   Host cuenta2
        HostName github.com
        User git
        IdentityFile ~/.ssh/guerreroski

Nota: una vez salvado el archivo hay que probar las llaves, primero borramos llaves de cache

/.ssh $ ssh-add -D ==> con esto borramos la cache
/.ssh $ ssh-add id_rsa
/.ssh $ ssh-add guerreroski ==> con esto agregamos nuevas llaves
/.ssh $ ssh -T cuenta1
/.ssh $ ssh -T cuenta2 ==> con esto hacemos test para saber si funcionan las llaves
/.ssh $ cd /clone
clone $ mkdir elpedroski-ven
clone $ mkdir guerreroski-col
clone $ cd elpedroski-ven
elpedroski-ven $ git init
        "      $ git remote add origin [git@cuenta1:elpedroski-universidadplatzi/blog-universidad]
        "      $ git pull origin master
        "      $ touch elpedroskivzla.txt
        "      $ git add -A
        "      $ git commit -m "elpedroskivzla"
        "      $ git push origin master
        "      $ cd ../guerreroski-col
guerreroski-col $ git init
        "       $ git remote add origin [git@cuenta2:elpedroski-universidadplatzi/blog-universidad]
        "       $ git pull origin master
        "       $ git config user.name "guerreroski-col"
        "       $ git config user.mail "pedroskiplgo@gmail.com"
        "       $ touch guerreroskicol.txt
        "       $ git add -A
        "       $ git commit -m "guerreroskicol"
        "       $ git push origin master

Nota: en el ejercicio trabajamos con "2" cuentas pero se pueden trabajar con "N" cuentas

********************************************************************************************************************************************************************************
     
##12. Workflow Dinamicos en equipos (ultimos pasos)

Nota: Elimino archivos innecesarios desde cuenta1 carpeta elpedroski-ven

elpedroski-ven $ rm deploy.txt
       "       $ rm index.html
       "       $ git add -A
       "       $ git commit -m "Eliminacion de archivos"
       "       $ git push origin master ==> muestra un error ![rejected] master==>master(fetch first)
       "       $ git fetch origin
       "       $ git merge origin/master
       "       $ git push origin master ==> el error desaparece, porque "git" exige hacer primero "git fetch" y "git merge"

Nota: entre tanto "guerreroski" revisa el repositorio en GitHub y se entera de la eliminacion de algunos archivos, entonces realiza lo siguiente:
 
guerreroski-col $ git fetch origin ==> descarga los cambios que hubo en el repositorio
        "       $ git merge origin/master ==> fusiona los cambios del repositorio con la rama origin/master de "guerreroski"
        "       $ rm elpedroskivzla.txt
        "       $ rm guerreroskicol.txt ==> "guerreroski" borra 2 archivos
        "       $ git fetch origin ==> no muestra nada porque cuenta1 no ha hecho mas cambios 
        "       $ git add -A
        "       $ git commit -m "Limpieza de archivos"
        "       $ git push origin master ==> ahora si los "2" archivos desaparecen en el repositorio en GitHub

guerreroski-col $ git checkout -b responsivedesign ==> creo una rama y me ubico en ella
        "       $ touch mobile.ccs
        "       $ git add -A
        "       $ git commit -m "Responsivedesign para mobile"
        "       $ git push origin responsivedesign ==> empujo el archivo a la rama en el GitHub
        "       $ touch tableta.ccs
        "       $ git add -A
        "       $ git commit -m "Responsivedesign para tableta"
        "       $ git push origin responsivedesign 

elpedroski-ven $ git fetch origin
        "      $ git branch -a ==> muestra las ramas y en donde estoy ubicado
        "      $ git checkout responsivedesign ==> me cambio a la rama responsivedesign
        "      $ git branch -a
        "      $ git checkout master ==> me cambio a la rama master para hacer la fusion
        "      $ git merge origin/master ==> sincronizo con la rama master lo que hizo "guerreroski"
        "      $ touch hello.js
        "      $ git add -A
        "      $ git commit -m "Se agrego hello.js"
        "      $ git fetch origin ==> observo que "guerreroski" no ha hecho mas archivos
        "      $ git push origin master ==> no hizo falta hacer el "merge"

guerreroski-col $ touch largescreens.ccs
        "       $ git add -A
        "       $ git commit -m "Se agrego largescreens"
        "       $ git push origin responsivedesign
        "       $ git checkout master
        "       $ git fetch origin
        "       $ git merge origin/master ==> sincronizo con la rama master lo que hizo "elpedroski"
        "       $ git merge responsivedesign ==> fusiono la rama con master
        "       $ git push origin master ==> empujo los archivos a la rama master de GitHub


Nota: Ahora desde la cuenta "guerreroski", siguiendo el curso elimino el FORK que tenía y lo vuelvo a crear

guerreroski-col $ git remote -v ==> muestra los remotos
        "       $ git remote rm origin  ==> elimino los remotos
        "       $ git remote add origin [git@cuenta2:guerreroski/blog-universidad.git] ==> cuenta forkeada
        "       $ git remote add upstream [git@cuenta2:elpedroski-universidadplatzi/blog-universidad.git] ==> cuenta principal del proyecto
        "       $ git pull origin master ==> baja cambios desde fork-repo
        "       $ git pull upstream master ==> baja cambios desde principal-repo
        "       $ touch mediaquery.ccs
        "       $ git add -A
        "       $ git commit -m "Media Query"
        "       $ git fetch upstream ==> verifico si hay cambios en el reposotorio principal
        "       $ git push origin master ==> lanzo los cambios al fork-repo

Nota: desde GitHub "guerreroski" obserbo el fork-repo y se encuentra el archivo "mediaquery.ccs", mientras en principal-repo no esta entonces hago lo siguiente:
      - doy clic al boton "New Pull Request"
      - aparece "Company Changes"
      - doy clic a "Create Pull Request"
      - Write: Inclusion de Media Query al proyecto principal
      - clic de nuevo a "Create Pull Request"
      - ahora desde la cuenta GitHub "elpedroski" observo que hay un Pull Request "Media Query"
      - doy clic a "Merge Pull Request"
      - doy clic a "Confirm Merge"
      - enter a "Media Query"

Nota: en esta ruta agrego y quito los colaboradores al proyecto si estan agregados pueden hacer "Merge" o "git push" sin pedir permiso (New Pull Request)
      Settings/Collaborators&teams.

*********************************************************************************************************************************************************************************

##13. Deploy Total en Sever. "Bare Repository"
      - Un "bare repository" unicamente guarda el historial de cambios, con todos sus archivos, pero NO puede editarlos como tal
      - Si por una circunstancia extrema, no se puede usar GitHub, entonces se tiene a "bare repositories"

clone $ mkdir server
  "   $ mkdir local
  "   $ cd server
server $ git init --bare repo.git
  "    $ cd local
local $ git clone ../server/repo.git repo
  "   $ cd repo
repo  $ touch README.md
  "   $ git add -A
  "   $ git commit -m "Documentacion"
  "   $ git remote -v ==> ya estoy conectado a remoto
  "   $ git push origin master

*********************************************************************************************************************************************************************************

##14. Git Tags y ecosistemas de proyectos en GitHub
      - Especificar momentos de tu repositorio importantes
      - Puede ser una nueva versión del proyecto, un feacture fuerte, etc
      - Abanderar commits. Para recordarlos, resaltarlos
      - Existen 2 formas de crear tags : 
               - Annotated Tags ==> genera un mensaje y la version
               - Lightweight Tags ==> no viene mensaje, solo la version

Master $ git init
  "    $ git add -A 
  "    $ git commit -m "Base de archivos"
  "    $ touch 1.txt
  "    $ git add -A
  "    $ git commit -m "1"
  "    $ touch 2.txt
  "    $ git add -A
  "    $ git commit -m "2"

 -Annotated Tag ==> git tag -a [version] -m [mensaje]

Master $ git tag -a v1.0 -m "Navegacion completa"
  "    $ git tag ==> nuestra "v1.0"
  "    $ git show v1.0 ==> muestra la vinculacion del tag con el ultimo commit el "2"
  "    $ touch archivo.txt
  "    $ git add -A
  "    $ git commit -m "Archivo"
  "    $ git superlog
  "    $ git checkout v1.0 ==> el head se mueve al tag indicado
  "    $ git checkout master ==> el head se mueve a la rama master

 -Lightweight Tag ==> git tag [version]

Master $ touch README.txt
  "    $ git add -A
  "    $ git commit -m "README"
  "    $ git tag v1.1
  "    $ git tag ==> muestra las versiones
  "    $ git superlog
  "    $ git checkout v1.0
  "    $ git checkout v1.1 ==> el head se mueve a la version indicada
  "    $ git tag  -l ==> muestra las versiones con tag
  "    $ git show v1.1 ==> muestra la informacion de la version
  "    $ git tag -a v0.9 [commit1] ==> se agrega un tag en un commit ancestral, ejemplo el "commit1"
  "    $ git checkout v0.9
  "    $ git superlog
  "    $ git checkout master

Nota: para buscar proyectos https://github.com/explore

*********************************************************************************************************************************************************************************

##. 15 Git Extras. 
1. ¿Para que sirve?. 
    - Agilidad
    - Es una extension de comandos en Git
    - Repositorio Open-Source

2. Requisitos en Linux
    - Debian y Ubuntu
      $ sudo apt-get install git-extras   

3. Requisitos Windows
   - www.brew.sh
   - En linea de comandos:
     git clone https://github.com/tj/git-extras.git
     git checkout $(git describe --tags $(git rev-list --tags --max-count=1)
   - Encuentra y ejecuta el archivo del clon:
     install.cmd

4. Requisitos en Mac
   - Homebrew
     www.brew.sh
   - En terminal
     brew install git-extras

5. ¿Donde esta?
   github.com/tj/git-extras

Ejemplo desde consola debian
 
$ mkdir extras | cd extras/
extras $ su root
extras # aptitude install git-extras ==> instalacion
  "    # git extras ==> muestra toda la informacion del comando
  "    # touch index.html
  "    # git add -A
  "    # git commit -m "Initial commit"
  "    # git summary 

*********************************************************************************************************************************************************************************

## 16. GitHub for Mac

- ¿Para que sirve?
- Para tener una visualizacion mas comoda del proyecto. Es una interfaz grafica de usuario. Es gratuita.
- ¿Como se descarga?
- https://desktop.github.com/

*********************************************************************************************************************************************************************************

## 17. GitHub for Windows

- https://desktop.github.com/

*********************************************************************************************************************************************************************************

## 18. Zenhub

- Zenhub.io
- ¿Como funciona?
- "Project Management" mas completo y de manera nativa, desde GitHub
- ¿Como se descarga?
- https://www.zenhub.io
- email principal de GitHub ==> get started
- add the extension to chrome or firefox
- get the extension 

Nota: Al buscar en GitHub el repositorio elpedroski-universidadplatzi/blog-universidad, aparece una nueva pestaña llamada Zzenhub. Hacemos clic en ella y aparecen varias
      columnas que a continuacion se explican:

1. Icebox ==> son todas esas ideas que uno tiene y aunque parezcan extrañas, deberiamos ejecutar algun dia, entonces se pueden ir cambiando Issues al Icebox para ejecutarlas
              en algun momento
2. Backlog ==> es donde se prioriza lo mas importante a desarrollar (feacture and bugs)
3. Feactures ==> elementos funcionales de una aplicacion, es decir, algo que hace la aplicacion. Ejemplo: realizar una pregunta, responderla, votar positiva o negativamente una 
                 pregunta, ver videos, tomar una foto, etc.
4. Bug ==> error de software. Es un problema en un programa del computador o sistema de software que desencadena un resultado indeseado
5. In Progress ==> es la parte donde una vez se definieron los Issues prioritarios a desarrollar, se colocan aqui
6. Review/QA ==> una vez se termine los Issues que estan en el In Progress, se pasan a Review/QA y si todo sale segun lo esperado se hace una fusion a la rama master y se pasa 
                 a la columna Done
7. Done

- Milestones o hito ==> es una tarea de duracion cero que simboliza el haber conseguido un logro importante en el proyecto. Los hitos son una forma de conocer el avance del 
                        proyecto, sin estar familiarizado con el mismo y constituyen un trabajo de duracion cero porque simbolizan un logro, un punto, un momento en el proyecto

- Issue ==> es la unidad de trabajo para realizar una mejora en un sistema informatico. Un issue puede ser el arreglo de un fallo, una caracteristica pedida, una tarea, un 
            pedido de documentacion especifico y cualquier tipo de solicitud al equipo de desarrollo   

*********************************************************************************************************************************************************************************

## 19. Gitlab

- Es un GitHub institucional, privado
- ¿Por que Gitlab?
- Hay empresas que necesitan seguridad 
## Instalacion primera opcion
   1. https://about.gitlab.com/downloads
   2. Entrar al servidor via ssh, dependiendo del sistema operativo
   3. Ejecuta los comandos
   4. ¿Problemas? 
   5. Contacta con soporte o el administrador de servidores

## Instalacion segunda opcion
   1. One-clic installer (Digital Ocean, Bitnami)
   2. Entrar via ssh al servidor y configurar

## Terminologia
  
GitHub usa diferentes terminos comparado a Gitlab
  
      GitHub                  Gitlab               ¿Que significa?

1. Pull Request          1. Merge Request        1. Peticion de fusion
2. Gist                  2. Snippet              2. "Pedazos de codigo"
3. Repository            3. Project              3. "Repo" de git discusiones y configuraciones
4. Organizations         4. Groups               4. "Group-Level" Management

*********************************************************************************************************************************************************************************

## 20. Git Ignore

- Ignorar archivos que no se registren en el repositorio central 

$ mkdir ignore ==> el nombre es aleatorio
ignore $ touch README.md
  "    $ git init
  "    $ git add -A
  "    $ git commit -m "Documentacion"
  "    $ touch mevasaignorar.txt
  "    $ touch .gitignore
Nota: editamos el archivo .gitignore y agrego lo siguiente:
      *.txt              
      mevasaignorar.txt  ==> si comento las 2 lineas entonces git SI lo rastrea
ignore $ git status ==> muestra solo el archivo .gitignore

Nota: coleccion completa de todos los archivos /gitignore
      https://github.com/github/gitignore

*********************************************************************************************************************************************************************************

## 21. Git Diff

- Es un comando para comparar commits 

$ mkdir proyectos | cd proyectos/
proyectos $ git init
   "      $ touch hola1.txt
   "      $ git add -A
   "      $ git commit -m "hola1"
   "      $ touch hola2.txt
   "      $ git add -A
   "      $ git commit -m "hola2"
   "      $ git log
   "      $ git diff [commit id hola1] ==> nunca se coloca el commit recien creado

- Comparando commits desde GitHub

  - Desde GitHub busco un repositorio por ejemplo: https://github.com/mikenieva/railstutorial
  - Entro a commit 
  - Enter a cualquier commit id
  - Enter a split

Nota: muestra del lado derecho lo que se agrego y del lado izquierdo lo que se quito

*********************************************************************************************************************************************************************************

## 22. Git Stash
 
- Git nos brinda muchos comandos para controlar las ramas una de ellas es el "git stash", que guarda las modificaciones locales en temporal y vuelve el directorio de trabajo a un estado inicial (como si no se hubiese hecho ningun cambio en la rama)

$ mkdir proyecto2 | cd proyecto2/
proyecto2 $ git init
   "      $ touch index.html
   "      $ git add -A
   "      $ git commit -m "Inicio de archivos"

Nota: edito el archivo index.html y agrego lo siguiente:
      Hola Mundo
      Responsive Design  ==> salvo y salgo

Nota: pero la gerencia me indica que se necesita integrar el comercio electronico ya mismo al proyecto2. Ok pero necesito no perder el trabajo hecho hasta ahora. Entonces se
      hace lo siguiente:
proyecto2 $ git status
   "      $ git stash ==> muestra saved working directory and index state wip on master: commit id "Inicio de archivos"

Nota: se acaban de guardar los archivos, index.html esta vacio

proyecto2 $ touch ecommerce.html
   "      $ git add -A
   "      $ git commit -m "ecommerce"
   "      $ git stash list
   "      $ git stash apply ==> regresa lo que tenia antes de ejecutar "git stash" ojo no genera commit, index.html recupera su informacion
   "      $ git add -A
   "      $ git commit -m "Cambios de stash"
   "      $ git log
   "      $ git superlog




























































































































































































 







        







































 









 



